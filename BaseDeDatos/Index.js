const mongoose = require ('mongoose')
const MONGO_URI = 'mongodb+srv://karla:PlxZOLGhRlzH@cluster0.dykfa.mongodb.net/myFirstDatabase?retryWrites=true&w=majority'

const conectarMongo = async () => {
    await mongoose.connect(MONGO_URI, {
      useNewUrlParser: true, 
      useUnifiedTopology: true,
    })
      .then(()=>{
        console.log('Se ha conectado a la Base de datos exitosamente')
      })
      .catch(err =>{
        console.error(err)
      })
}
  
module.exports = { conectarMongo }