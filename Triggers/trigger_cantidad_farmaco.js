const { DetalleVenta } = require('../Esquematización')

let cantidad_maxima = 10

module.exports = trigger_cantidadfarmaco = (farmaco) => {

  const { detav_cantidad } = farmaco

  try {
    if(detav_cantidad > cantidad_maxima){
      throw 'Excede el pedido máximo del fármaco'
    } else {

      const insertar = new DetalleVenta(farmaco)
      insertar.save()

      console.log('Se insertó el registro correctamente')
      console.log(farmaco)
    }
  } catch (e) {
    console.log(e)
  }
}