const mongoose = require('mongoose')
const { Schema } = mongoose

const EstadoVendedorSchema = new Schema({

    estado_id: {
        type: Number,
        require: true
    },
    estado_descripcion: {
        type: String,
        require: true
    }
    
})

module.exports = mongoose.model('estadovendedor', EstadoVendedorSchema)