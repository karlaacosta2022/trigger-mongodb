const mongoose = require('mongoose')
const { Schema } = mongoose

const SucursalSchema = new Schema({

    suc_id: {
        type: Number,
        require: true
    },
    suc_nombre: {
        type: String,
        require: true
    },
    suc_direccion: {
        type: String,
        require: true
    },
    suc_telefono: {
        type: String,
        require: true
    },
    activo_convenio: {
        type: Boolean,
        require: true
    }
    
})

module.exports = mongoose.model('sucursal', SucursalSchema)