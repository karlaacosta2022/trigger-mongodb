const mongoose = require('mongoose')
const { Schema } = mongoose

const HorarioLaboralSchema = new Schema({

    horario_id: {
        type: Number,
        require: true
    },
    horario_entrada: {
        type: Date,
        require: true
    },
    horario_salida: {
        type: Date,
        require: true
    },
    pago_hora: {
        type: Number,
        require: true
    },
    pago_horaextra: {
        type: Number,
        require: true
    },
    pago_total: {
        type: Number,
        require: true
    }
    
})

module.exports = mongoose.model('horariolaboral', HorarioLaboralSchema)