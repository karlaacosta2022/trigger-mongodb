const mongoose = require('mongoose')
const { Schema } = mongoose

const ProveedorSchema = new Schema({

    prov_id: {
        type: Number,
        require: true
    },
    prov_nombre: {
        type: String,
        require: true
    },
    prov_direccion: {
        type: String,
        require: true
    },
    prov_telefono: {
        type: String,
        require: true
    },
    prov_email: {
        type: String,
        require: true
    }
    
})

module.exports = mongoose.model('proveedor', ProveedorSchema)