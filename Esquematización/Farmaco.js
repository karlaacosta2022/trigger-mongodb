const mongoose = require('mongoose')
const { Schema } = mongoose

const FarmacoSchema = new Schema({

    farmaco_id: {
        type: Number,
        require: true
    },
    farmaco_nombre: {
        type: String,
        require: true
    },
    farmaco_precio: {
        type: Number,
        require: true
    },
    farmaco_fechavencimiento: {
        type: Date,
        require: true
    },
    farmaco_stock: {
        type: Number,
        require: true
    }
    
})

module.exports = mongoose.model('farmaco', FarmacoSchema)