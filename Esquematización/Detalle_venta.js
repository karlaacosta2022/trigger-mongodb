const mongoose = require('mongoose')
const { Schema } = mongoose

const DetalleVentaSchema = new Schema({

    detav_id: {
        type: Number,
        require: true
    },
    venta_id: {
        type: Number,
        require: true
    },
    cli_id: {
        type: Number,
        require: true
    },
    farmaco_id: {
        type: Number,
        require: true
    },
    ven_id: {
        type: Number,
        require: true
    },
    detav_cantidad: {
        type: Number,
        require: true
    }
})

module.exports = mongoose.model('detalleventa', DetalleVentaSchema)