module.exports = {

    AquisicionFarmaco: require('./Adquisicion_farmaco'),
    Cliente: require('./Cliente'),
    DetalleVenta: require('./Detalle_venta'),
    EstadoVendedor: require('./Estado_vendedor'),
    Farmaceutica: require('./Farmaceutica'),
    HorarioLaboral: require('./Horario_laboral'),
    Proveedor: require('./Proveedor'),
    Sucursal: require('./Sucursal'),
    Vendedor: require('./Vendedor'),
    Venta: require('./Vendedor')

  }