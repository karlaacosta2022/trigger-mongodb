const mongoose = require('mongoose')
const { Schema } = mongoose

const VentaSchema = new Schema({

    venta_id: {
        type: Number,
        require: true
    },
    venta_fecha: {
        type: Date,
        require: true
    },
    venta_descuento: {
        type: Number,
        require: true
    }
    
})

module.exports = mongoose.model('venta', VentaSchema)