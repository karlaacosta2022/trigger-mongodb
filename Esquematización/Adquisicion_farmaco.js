const mongoose = require('mongoose')
const { Schema } = mongoose

const AdquisicionFarmacoSchema = new Schema({

    adqui_id: {
        type: Number,
        require: true
    },
    adqui_fecha: {
        type: Date,
        require: true

    },
    adqui_costo: {
        type: Number,
        require: true
    }

})

module.exports = mongoose.model('adquisiciónfarmaco', AdquisicionFarmacoSchema)