const mongoose = require('mongoose')
const { Schema } = mongoose

const ClienteSchema = new Schema({

    cli_id: {
        type: Number,
        require: true
    },
    cli_cedula: {
        type: String,
        require: true
    },
    cli_nombres: {
        type: String,
        require: true
    },
    cli_apellidos: {
        type: String,
        require: true
    },
    cli_direccion: {
        type: String,
        require: true
    },
    cli_telefono: {
        type: String,
        require: true
    },
    cli_email: {
        type: String,
        require: true
    },
    cli_fechanacimiento: {
        type: Date,
        require: true
    },
    cli_prescripcion: {
        type: Boolean,
        require: true
    }

})

module.exports = mongoose.model('cliente', ClienteSchema)