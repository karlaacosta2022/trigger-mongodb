const mongoose = require('mongoose')
const { Schema } = mongoose

const FarmaceuticaSchema = new Schema({

    farma_id: {
        type: Number,
        require: true
    },
    farma_nombre: {
        type: String,
        require: true
    },
    farma_direccion: {
        type: String,
        require: true
    },
    farma_telefono: {
        type: String,
        require: true
    }
    
})

module.exports = mongoose.model('farmaceutica', FarmaceuticaSchema)