const mongoose = require('mongoose')
const { Schema } = mongoose

const VendedorSchema = new Schema({

    ven_id: {
        type: Number,
        require: true
    },
    ven_nombres: {
        type: String,
        require: true
    },
    ven_apellidos: {
        type: String,
        require: true
    },
    ven_fechanacimiento: {
        type: Date,
        require: true
    },
    ven_fechaingreso: {
        type: Date,
        require: true
    },
    ven_supervisor: {
        type: Number,
        require: true
    }
    
})

module.exports = mongoose.model('vendedor', VendedorSchema)